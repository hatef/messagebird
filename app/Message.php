<?php

namespace App;

use MessageBird\Client as Bird;
use bandwidthThrottle\tokenBucket\Rate;
use bandwidthThrottle\tokenBucket\TokenBucket;
use bandwidthThrottle\tokenBucket\storage\FileStorage;
use App\Util\MessageCounter;

/**
 * Message class.
 *
 * @package App
 */
class Message {
    
    /**
     *  MessageBird API Key (ideally this should go to an .env file)
     */
    const MB_API_KEY = 'op0ZyqsAbnBTEGuH0dQfCpndb';

    /**
     * The send request rate limit to MessageBird
     */
    const MB_API_REQ_PER_SEC = 1;

    /**
     * Construtor
     */
    public function __construct() {
        $this->messageInputFilter = new MessageInputFilter();
    }

    /**
     * Receives and validates a message
     *
     * @param string $request
     * @return array
     */
    public function receive(string $request): array
    {
        $validatedRequest = $this->messageInputFilter->validate(json_decode($request, true));

        if (isset($validatedRequest['error'])) {
            http_response_code(400);
            return $validatedRequest['error'];
        }

        return $this->throttler($validatedRequest);
    }

    /**
     * Sends the validated received message to MessageBird
     *
     * @param array $request
     * @return array
     */
    public function send(array $request): array
    {
        $MessageBird         = new Bird(self::MB_API_KEY);
        $Message             = new \MessageBird\Objects\Message();
        $Message->body       = $request['message'];
        $Message->originator = $request['originator'];
        $Message->recipients = [$request['recipient']];

        if ($request['encoding'] === MessageCounter::UTF16) {
            $Message->datacoding = 'unicode';
        }

        try {
            $MessageBird->messages->create($Message);
            return ['success' => 'Your message was sent to MessageBird!'];
        } catch (\MessageBird\Exceptions\AuthenticateException $e) {
            return ['error' => 'Your accessKey is unknown!'];
        } catch (\MessageBird\Exceptions\BalanceException $e) {
            return ['error' => 'Sorry but you are out of credits!'];
        } catch (\Exception $e) {
            return ['error' => 'We were unable to send this message to MessageBird!'];
        }
    }

    /**
     * Throttles the flow of sending message to the MessageBird API
     *
     * @param array $request
     * @return array
     */
    private function throttler(array $request): array
    {
        $storage = new FileStorage(__DIR__ . "/api.bucket");
        $rate    = new Rate(self::MB_API_REQ_PER_SEC, Rate::SECOND);
        $bucket  = new TokenBucket(self::MB_API_REQ_PER_SEC, $rate, $storage);
        $bucket->bootstrap(self::MB_API_REQ_PER_SEC);
        
        if (!$bucket->consume(1, $seconds)) {
            usleep(1000000);
        }

        return $this->send($request);
    }
}
