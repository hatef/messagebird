<?php

namespace App;

use App\Util\MessageCounter;

/**
 * MessageInputFilter class.
 *
 * @package App
 */
class MessageInputFilter {
    
    /**
     * Max allowed message length.
     */
    const MAX_MESSAGE_LNEGTH = 1;

    /**
     * Validates a received message
     *
     * @param array $request
     * @return array
     */
    public function validate(array $request): array
    {
        $return = $request;

        if (trim($request['originator']) === '') {
            $return['error'][] = 'Originator can\'t be empty!';
        }

        if (!is_numeric($request['recipient'])) {
            $return['error'][] = 'Recipient must be a valid phone number!';
        }

        if (trim($request['message']) === '') {
            $return['error'][] = 'Message body can\'t be empty!';
        } else {
            $countedMessages = (new MessageCounter)->count($request['message']);
            $return['encoding'] = $countedMessages->encoding;

            if ($countedMessages->no_of_messages > self::MAX_MESSAGE_LNEGTH)  {
                $return['error'][] = "You can only send a message of length 1 at the moment, based on the characters you used, your message length is ". $countedMessages->no_of_messages;
            }
        }

        return $return;
    }
}
