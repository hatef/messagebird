<?php

require 'vendor/autoload.php';
header('Content-Type: application/json');

use App\Message;

if ($_SERVER['REQUEST_METHOD'] !== "POST") {
    // No other HTTP method is allowed
    http_response_code(405);
    echo json_encode(['error' => 'HTTP method not supported!']);
}

try {
    $msg = new Message();
    echo json_encode(
        $msg->receive(file_get_contents("php://input"))
    );
} catch(\Exception $e) {
    echo json_encode(['error' => 'Something went terribly wrong!']);
}
