<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Message;
use App\MessageInputFilter;

/**
 * Test Message.
 */
class TestMessage extends TestCase {

    public function testBadRequestReturnsCorrectHTTPCode()
    {
        $request = '{
            "recipient":31634080599,
            "originator":"",
            "message": "Howdy!"
        }';

        $message = new Message();
        $message->receive($request);
        $this->assertEquals(400, http_response_code());
    }

    public function testSendMethodIsCalled()
    {
        $request = '{
            "recipient":31634080599,
            "originator":"MessageBird",
            "message": "Hey there!"
        }';

        $request2 = '{
            "recipient":31634080599,
            "originator":"MessageBird",
            "message": "Again, hey there!"
        }';

        $badCode = $this->getMockBuilder(Message::class)
            ->setMethods(['send'])
            ->getMock();

        $badCode->expects($this->exactly(2))
            ->method('send');

        $badCode->receive($request);
        $badCode->receive($request2);

    }
}
