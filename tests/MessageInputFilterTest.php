<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\MessageInputFilter;

/**
 * Test MessageInputFilter.
 */
class TestMessageInputFilter extends TestCase {

    public function testValidRequest()
    {
        $request = [
            'recipient' => 191919191,
            'originator' => 191919191,
            'message' => 'Hey there'
        ];

        $validatedMessage = (new MessageInputFilter)->validate($request);
        $this->assertArrayNotHasKey('error', $validatedMessage);
    }

    public function testInvalidRequest()
    {
        $request = [
            'recipient' => ' ',
            'originator' => ' ',
            'message' => ' '
        ];

        $validatedMessage = (new MessageInputFilter)->validate($request);
        $this->assertArrayHasKey('error', $validatedMessage);
    }

    public function testRequestWithEmptyRecipient()
    {
        $request = [
            'recipient' => 'A',
            'originator' => 191919191,
            'message' => 'Hey there'
        ];

        $validatedMessage = (new MessageInputFilter)->validate($request);
        $this->assertSame('Recipient must be a valid phone number!', $validatedMessage['error'][0]);
    }

    public function testRequestWithEmptyOriginator()
    {
        $request = [
            'recipient' => 191919191,
            'originator' => ' ',
            'message' => 'Hey there'
        ];

        $validatedMessage = (new MessageInputFilter)->validate($request);
        $this->assertSame('Originator can\'t be empty!', $validatedMessage['error'][0]);
    }

    public function testRequestWithEmptyMessageBody()
    {
        $request = [
            'recipient' => 191919191,
            'originator' => 191919192,
            'message' => ' '
        ];

        $validatedMessage = (new MessageInputFilter)->validate($request);
        $this->assertSame('Message body can\'t be empty!', $validatedMessage['error'][0]);
    }

    public function testRequestWithLongMessageBody()
    {
        $request = [
            'recipient' => 191919191,
            'originator' => 191919191,
            'message' => 'Hey there, this is a message with lots of brackets {}{{{}{{{
                {{{{{{{{{{{}{{{{{{{{{{{{}}}}{{{}}}}{{{}}}}{{{{}}}}{{{}}'
        ];

        $validatedMessage = (new MessageInputFilter)->validate($request);
        $this->assertSame('You can only send a message of length 1 at the moment, based on the characters you used, your message length is 2', $validatedMessage['error'][0]);
    }
}
