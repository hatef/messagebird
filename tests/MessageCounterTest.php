<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Util\MessageCounter;

/**
 * Test MessageCounter.
 */
class TestMessageCounter extends TestCase {

    public function testMessageEncodingWithGSMChars()
    {
        $message = "Hey there";
        $countedMessages = (new MessageCounter)->count($message);
        $this->assertSame(MessageCounter::GSM_7BIT, $countedMessages->encoding);
        $this->assertSame(MessageCounter::GSM_7BIT_LEN, $countedMessages->per_message);
    }

    public function testMessageEncodingWithExtendedGSMChars()
    {
        $message = "{ Hey there }";
        $countedMessages = (new MessageCounter)->count($message);
        $this->assertSame(MessageCounter::GSM_7BIT_EX, $countedMessages->encoding);
        $this->assertSame(MessageCounter::GSM_7BIT_EX_LEN, $countedMessages->per_message);
    }

    public function testMessageEncodingWithEmojiChars()
    {
        $message = "Hey there 😀";
        $countedMessages = (new MessageCounter)->count($message);
        $this->assertSame(MessageCounter::UTF16, $countedMessages->encoding);
        $this->assertSame(MessageCounter::UTF16_LEN, $countedMessages->per_message);
    }
    
    public function testMultipartMessageCountWithGSMChars()
    {
        $message = "Hey there, this is gonna be a really long message ! I'm actually out of words so
                    am just gonna continue with orem ipsum quia dolor sit amet, consectetur.";
        $countedMessages = (new MessageCounter)->count($message);
        $this->assertSame(MessageCounter::GSM_7BIT_LEN_MULTIPART, $countedMessages->per_message);
        $this->assertSame(2, $countedMessages->no_of_messages);
    }

    public function testMultipartMessageCountWithExtendedGSMChars()
    {
        $message = "Hey there, with lots of brackets {}{{{}{{{{{{{{{{{{{{}{{{{{{{{{{{{}}}
                    }{{{}}}}{{{}}}}{{{{}}}}{{{}}";
        $countedMessages = (new MessageCounter)->count($message);
        $this->assertSame(MessageCounter::GSM_7BIT_EX_LEN_MULTIPART, $countedMessages->per_message);
        $this->assertSame(2, $countedMessages->no_of_messages);
    }

    public function testMultipartMessageCountWithEmojiChars()
    {
        $message = "Hey there, this is gonna be a really long message, maybe even two messages! 😀";
        $countedMessages = (new MessageCounter)->count($message);
        $this->assertSame(2, $countedMessages->no_of_messages);
        $this->assertSame(MessageCounter::UTF16_LEN_MULTIPART, $countedMessages->per_message);
    }
}
