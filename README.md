# Messagebird App

## About
An API application that accepts SMS messages submitted via a POST request containing a JSON object as 
request body and 
sends the received message to the MessageBird REST API using one of our REST API libraries.

## Todo
- Make env file
- More tests